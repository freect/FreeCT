# License for FreeCT's use in Academic Research 

**Please note that this license does not cover use of FreeCT for industry.  Please contact John Hoffman (johnmarianhoffman@gmail.com) directly if you are interested in using FreeCT for research or deployment in industry.** 

Copyright - 2016-2022 - John M. Hoffman

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

4. **Users of FreeCT software for research and academic work, including (but not limited to) data generation, derivative software, example images, etc. must cite this repository as well as any appropriate publications related to this software.  A list of these publications is provided at the end of this document.**

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## FreeCT and related Publications:

1. J. Hoffman, S. Young, F. Noo, & M. McNitt-Gray, *Technical Note: FreeCT_wFBP: A robust, efficient, open-source implementation of weighted filtered backprojection for helical, fan-beam CT.* Med. Phys., vol. 43, pp. 1411-1420, (2016).

2. J. Hoffman, F. Noo, S. Young, S. Hsieh, M. McNitt-Gray, *Technical Note: FreeCT_ICD: An open-source implementation of a model-based iterative reconstruction method using coordinate descent optimization for CT imaging investigations.* Med. Phys. vol. 45, pp. 3591–3603, (2018).

3. J. Hoffman, N. Emaminejad, M. Wahi-Anwar, G. H. Kim, M. Brown, S. Young, & M. McNitt-Gray. *Technical Note: Design and implementation of a high‐throughput pipeline for reconstruction and quantitative analysis of CT image data.* Med. Phys. vol. 46, pp. 2310–2322, (2019).
