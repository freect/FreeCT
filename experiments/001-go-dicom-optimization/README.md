# Go-DICOM optimization

## Purpose

This experimental setup should be used to measure optimizations to FreeCT's [go-dicom](https://gitlab.com/freect/go-dicom) implementation.

## Assumptions and Prerequisites

Ensure you have Python installed, and the [go tool](https://go.dev/dl).

We assume that there are two key branches:

- `org-main` which contains essentially the upstream read code (from https://github.com/suyashkumar/dicom)
- `main` which contains all optimizations to be tested

We use the `fct-raw-util`'s `timedread` command.

All output is saved to `results.csv`.

You'll need to have a copy of `gitlab.com/freect/go-dicom` cloned into the same top level directory as your FreeCT repo: (i.e. `/path/to/code/go-dicom` and `/path/to/code/freect` and you'll need to ensure that your `go.mod` for FreeCT contains the following line:

```
replace gitlab.com/freect/go-dicom => /Users/jhoffman/Code/go-dicom
```

this ensures that when we check out the `main` and `org-main` branches, the changes will be reflected in our tests.

### Data

You'll need one case of the [TCIA LDCT projection data](https://wiki.cancerimagingarchive.net/pages/viewpage.action?pageId=52758026) and the same case converted to the single-file file version. These paths can be modified for your system in `run-experiment.py`.

FreeCT can do a conversion for you if we haven't been able to make the converted data publicly available:

```
./fct-raw-util convert --input.battelle /path/to/ldct/case/ --output.sfctpd /path/to/converted-case.dcm
```

## Running the experiment

Ensure all prereqs are met, then:

```
python run-experiments.py
```
