# FreeCT Experiments

FreeCT and supporting code are intended to be scientific tools and as such, we believe in making all of our experiments and code publicly available.  This "experiments" folder is intended for us to capture the experiments that we do to measure performance, ensure correct functioning of our code, and provide an auditable record of work.

A couple of thoughts:

- None of the code here can be depended on by the rest of the repository (including other experiments, better to copy a script)
- Make sure that each experiment has its own directory
- Directories should have an index and a name to hint at what they do (e.g., 001-ldct-read-profiling)
- Each experiment should have a README.md briefly (or lengthily) describing what the experiment is, and if it was used for an abstract, conference, or paper submission
- If an experiment is used for a paper or abstract submission, it should not be modified after submission unless as part of the review process. This will aid with repeatability.

Finally, if something should be maintained as part of the repository (for instance we find an experiment would actually make a good regression test or something) it should be promoted out of the experiments directory and into the main repository.  Although it's a "nice to have" that these experiments run long into the future, we generally will not go out of our way to ensure that they are always backwards compatible.

We should add tags on the versions of FreeCT that are used to run the final experiments. E.g., `git tag -a "experiment-001" -m "version used for AAPM submission 2023"`

None of the above is set in stone, so if we find it's not working, we'll adjust and update this readme.