/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	
	"github.com/spf13/cobra"
	
	log "github.com/sirupsen/logrus"
	
	"fmt"
	"path/filepath"
	"os"
	"strings"
	"io"
)

var EXE_PATH = "D:/ReconCT_14.2.0.40998/ReconCT.exe"

var s *CLISettings
	
// reconCtCmd represents the reconCt command
var reconCtCmd = &cobra.Command{
	Use:   "recon-ct",
	Short: "A brief description of your command",
	Long: `Run a reconstruction on ReconCT`,
	Run: func(cmd *cobra.Command, args []string) {

		if len(args) < 1 {
			cmd.Usage()
			log.Fatal("Must past PTR/IMA file as last argument")
		}
		
		ptr_file := args[0]

		// Required stuff
		// patientname
		if s.patientName == "" {
			cmd.Usage()
			log.Fatal("must specify patient name.")
		}

		// patientid
		if s.patientID == "" {
			cmd.Usage()
			log.Fatal("must specify patient id.")
		}

		// an algorithm (--wfbp/--safire/--admire)
		algs, err := s.GetAlgorithms()
		if err != nil {
			cmd.Usage()
			log.Fatal("algs failed:", err)
		}

		// Optional stuff
		
		// fov
		// 
		// fov behavior is still a little wonky.
		// I'm not sure it's currently reading a sane default from the
		// field if we don't provide on.
		var fov []float64
		if cmd.Flags().Lookup("fov").Changed {
			fov, err = s.GetFOV()
			if err != nil {
				log.Fatal("fov failed:", err)
			}
		} else {
			fov = []float64{0.0}
		}

		// dose as a percentage of the og dose
		var dose []float64
		if cmd.Flags().Lookup("dose").Changed {
			dose, err = s.GetDose()
			if err != nil {
				log.Fatal("dose failed:", err)
			}
		} else {
			dose = []float64{100.0}
		}

		// kernel (defaults to smoothest available if not provided)
		kernel, err := s.GetKernel()
		if err != nil {
			log.Fatal("kernel failed:", err)
		}

		// defaults to 1.0 if not provided (seems like at least?)
		slice_thickness, err := s.GetSliceThickness()
		if err != nil {
			log.Fatal("slice thickness failed:", err)
		}

		// Position/increments
		// becuase start/end position can be validly set to 0.0, we use a pointer
		// to allow us to indicate "unset"
		var start_pos *float64 = nil
		if cmd.Flags().Lookup("start").Changed {
			start_pos = &s.start
		}

		var end_pos *float64 = nil
		if cmd.Flags().Lookup("end").Changed {
			end_pos = &s.end
		}

		center_x, center_y := s.centerX, s.centerY
		
		// Not validly set to zero so we can pass without worrying too much (omitempty would behave correctly)
		increment := s.increment

		// Offensively deep nesting happening here.
		for curr_alg := range algs {
			for curr_fov := range fov {
				for curr_dose := range dose {
					for curr_kernel := range kernel {
						for curr_st := range slice_thickness {
							
							data, err := GetXML(
								dose[curr_dose],
								kernel[curr_kernel],
								slice_thickness[curr_st],
								algs[curr_alg],
								fov[curr_fov],
								s.patientName,
								s.patientID,
								start_pos,
								end_pos,
								increment,
								center_x,
								center_y,
							)

							if err != nil {
								log.Fatal("conversion to xml failed: ", err)
							}

							// Save xml configuration for handoff to reconct
							config_name := fmt.Sprintf("%s-%0.1f-%s-%s-%0.2f.xml", s.patientName, dose[curr_dose], strings.Replace(algs[curr_alg], "Standard - ", "", -1), kernel[curr_kernel], slice_thickness[curr_st])

							config_filepath := filepath.Join(s.imageOutput, config_name)
							
							f, err := os.Create(config_filepath)
							if err != nil {
								log.Error("failed to save xml:", err)
								continue
							}
							
							_, err = f.Write(data)
							if err != nil {
								log.Error("failed to save xml:", err)
								continue
							}

							f.Close()
							
							// Call the reconct exe on the saved xml
							f_out, err := os.Create(config_filepath + ".out")
							if err != nil {
								log.Error("failed to save xml:", err)
								continue
							}

							// Create an output pipe that will print
							// to cli and a file.  NOTE: we may want
							// to nerf the stdout output once this
							// actually starts running.
							output_pipe := io.MultiWriter(f_out, os.Stdout)
							
							cmd :=  GetShellCommand(EXE_PATH, ptr_file, config_filepath, s.imageOutput)

							fmt.Println("executing command: ", cmd.String())

							// Capture both stdout and stderr to the output pipe
							cmd.Stdout = output_pipe
							cmd.Stderr = output_pipe

							err = cmd.Run()
							if err != nil {
								log.Error("error calling reconct: ", err)
							}
							
							f_out.Close()
							
						}
					}
				}
			}
		}

		// Clean up the xml

	},
}

func init() {

	val, exists := os.LookupEnv("RECONCT")
	if exists {
		EXE_PATH = val
	}
	
	s = DefaultCLISettings()
	
	rootCmd.AddCommand(reconCtCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// reconCtCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// reconCtCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

	// Because this tool is highly specialized, we manually generate the required flags
	reconCtCmd.Flags().BoolVar(&s.useWFBP, "wfbp", s.useWFBP, "Use WFBP reconstruction algorithm")
	reconCtCmd.Flags().BoolVar(&s.useSAFIRE, "safire", s.useSAFIRE, "Use SAFIRE reconstruction algorithm")
	reconCtCmd.Flags().BoolVar(&s.useADMIRE, "admire", s.useADMIRE, "Use ADMIRE reconstruction algorithm")
	reconCtCmd.Flags().StringVar(&s.imageOutput, "imageoutput", s.imageOutput, "Output directory for reconstructed images")
	//DO NOT USE (does not work) reconCtCmd.Flags().BoolVar(&s.anonymize, "anonymize", s.anonymize, "Anonymize the output dicom images (default True)")
	reconCtCmd.Flags().StringVar(&s.sliceThickness, "slicethickness", s.sliceThickness, "Slice thickness(es) to reconstruction with")
	reconCtCmd.Flags().StringVar(&s.dose, "dose", s.dose, "Dose(s) to reconstruction with")
	reconCtCmd.Flags().StringVar(&s.kernel, "kernel", s.kernel, "Kernel(s) to reconstruction with. Note: make sure kernels are compatible with algorithm selection.")
	reconCtCmd.Flags().StringVar(&s.fov, "fov", s.fov, "Field of view to use for reconstructions (only supports a single value, defaults to 275mm)")
	reconCtCmd.Flags().StringVar(&s.output, "output", "stdout", "Where to save output. Default prints to stdout.")
	reconCtCmd.Flags().StringVar(&s.patientName, "patientname", "", "Patient name to use. gets set in output dicom.  Required.")
	reconCtCmd.Flags().StringVar(&s.patientID, "patientid", "", "Patient ID to use. gets set in output dicom.  Required.")

	// Position stuff
	reconCtCmd.Flags().Float64Var(&s.start, "start-pos", 0.0, "start location of reconstruction")
	reconCtCmd.Flags().Float64Var(&s.end, "end-pos", 0.0, "end location of reconstruction")
	reconCtCmd.Flags().Float64Var(&s.increment, "increment", 0.0, "spacing between reconstructed slices (note: NOT slice thickness)")
}
