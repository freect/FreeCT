/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/suyashkumar/dicom"
	"github.com/suyashkumar/dicom/pkg/tag"

	log "github.com/sirupsen/logrus"
	"path/filepath"
	"strings"
	"os"
	"bytes"
)

func replaceExtension(fp string) string {
	return strings.TrimSuffix(fp, filepath.Ext(fp)) + ".ptr"
}

var retain_dicom bool
var dicom_buffer = 10_000_000 // 10MB buffer size to caputre the dicom header can't imagine it would need to be any bigger, but raw data is weird.

// imaToPtrCmd represents the imaToPtr command
var imaToPtrCmd = &cobra.Command{
	Use:   "ima-to-ptr",
	Short: "Convert an IMA file to a PTR (IYKYK)",
	Run: func(cmd *cobra.Command, args []string) {
		
		ima_path := args[0]
		fmt.Printf("attempting to convert %v to PTR...\n", ima_path)
		
		ds, err := dicom.ParseFile(ima_path, nil)
		if err != nil {
			log.Fatal("failed to parse ima header: ", err)
		}

		elem, err := ds.FindElementByTag(tag.Tag{0x7fe1, 0x1010})

		data := elem.Value.GetValue().([]byte)

		fmt.Printf("read some data: %v...\n", data[0:60])

		var ptr_path string
		if len(args) > 1  {
			ptr_path = args[1]
		} else {
			ptr_path = replaceExtension(ima_path)
		}

		// Save the output
		fmt.Printf("saving to %s...\n", ptr_path)
		
		ptr_file, err := os.Create(ptr_path)
		if err != nil {
			log.Fatal("failed to create ptr: ", err)
		}

		// Write primary project data
		_, err = ptr_file.Write(data)
		if err != nil {
			log.Fatal("failed to save ptr: ", err)
		}

		// Save the header at the end of the file, if requested
		// TODO: maybe reimplement with bufio.Scanner?
		if retain_dicom {
			fmt.Println("retaining dicom header from ima... (--retain-dicom)")
			
			// open and read a healthly chunk of data
			header_data := make([]byte, dicom_buffer)

			ima_file, err := os.Open(ima_path)
			if err != nil {
				log.Fatalf("reopening ima to extract header failed: %s", err)
			}

			n, err := ima_file.Read(header_data)
			if err != nil {
				log.Fatalf("reading header from ima failed: %s", err)
			}

			fmt.Println(n)

			magic_word := []byte{0x58, 0x41, 0x43, 0x42}

			fmt.Println("looking for magic word: ", string(magic_word))
			
			substrings := bytes.SplitN(header_data, magic_word, 2)
			
			if len(substrings) < 1 {
				log.Fatalf("looking for xacb magic word: %s", err)
			}
			
			fmt.Printf("read some header data... %s\n", string(substrings[0][0:256]))

			//fmt.Println("header:", string(header_data[0:256]))

			n, err = ptr_file.Write(substrings[0])
			if err != nil {
				log.Fatalf("failed to append dicom header to ptr file: %s", err)
			}

			fmt.Printf("wrote %d header bytes into ptr file\n", n)
			
		}

		ptr_file.Close()

	},
}

func init() {
	rootCmd.AddCommand(imaToPtrCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// imaToPtrCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	imaToPtrCmd.Flags().BoolVar(&retain_dicom, "retain-dicom", false, "retain dicom after conversion (transfers phi)")
}
