/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"
	"time"

	"github.com/spf13/cobra"

	log "github.com/sirupsen/logrus"
)

var N int
var note string

// timedreadCmd represents the timedread command
var timedreadCmd = &cobra.Command{
	Use:   "timedread",
	Short: "read a dataset and report time in seconds",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {

		for i := 0; i < N; i++ {

			r, err := in.GetReader()
			if err != nil {
				log.Fatal(err)
			}

			start := time.Now()
			_, _, _, err = r.Read(nil)
			if err != nil {
				log.Fatal(err)
			}

			if note != "" {
				fmt.Printf("%s,%v,%s\n", r, time.Since(start), note)
			} else {
				fmt.Printf("%s,%v\n", r, time.Since(start))
			}

		}
	},
}

func init() {
	rootCmd.AddCommand(timedreadCmd)
	GenerateInputFlags(timedreadCmd, in)
	timedreadCmd.Flags().IntVar(&N, "repeat", 1, "perform N timed reads of the same dataset")
	timedreadCmd.Flags().StringVar(&note, "note", "", "add a note to CSV output (e.g., 'timing results for default go-dicom 1')")
}
