package cmd

import (
	"encoding/xml"
	"fmt"
	"hash/fnv"
	"math/rand"
	//"path/filepath"
	"os"
	"os/exec"
	"strings"
)

// Helper function
func hash(s string) string {
	h := fnv.New32a()
	h.Write([]byte(s))
	return fmt.Sprintf("%x", h.Sum32())
}

// This dedicated type is required to serialize SliceWidth for ReconCT into two decimal places
type TwoDecimalFloat float64

func (f TwoDecimalFloat) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	s := fmt.Sprintf("%0.2f", f)
	return e.EncodeElement(s, start)
}

type ReconJobViewModel struct {
	ReconMode            string           `xml:"ReconMode,omitempty"`
	ConvolutionKernel    string           `xml:"ConvolutionKernel,omitempty"`
	SliceWidth           TwoDecimalFloat  `xml:"SliceWidth,omitempty"`
	SeriesDescription    string           `xml:"SeriesDescription,omitempty"`
	FieldOfView          float64          `xml:"FieldOfView,omitempty"`
	AnonymizeDicomImages bool             `xml:"AnonymizeDicomImages,omitempty"`
	PatientName          string           `xml:"PatientName,omitempty"`
	PatientId            string           `xml:"PatientId,omitempty"`
	NoiseSimulation      *NoiseSimulation `xml:"NoiseSimulation,omitempty"`

	ReconRange *ReconRange `xml:"ReconRange,omitempty"`
	CenterX    float64     `xml:"CenterX,omitempty"`
	CenterY    float64     `xml:"CenterY,omitempty"`
}

type ReconRange struct {
	Increment     float64 `xml:"Increment,omitempty"`
	StartPosition float64 `xml:"StartPosition,omitempty"`
	EndPosition   float64 `xml:"EndPosition,omitempty"`
}

type NoiseSimulation struct {
	NoiseSimulationChecked         bool   `xml:"NoiseSimulationChecked"`
	NoiseSimulationItems           string `xml:"NoiseSimulationItems"`
	NoiseSimulationReductionFactor int    `xml:"NoiseSimulationReductionFactor"`
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandString(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func GetXML(
	dose_pct float64,
	kernel string,
	slice_thickness float64,
	algorithm string,
	fov float64,
	patient_name string,
	patient_id string,
	start_pos *float64,
	end_pos *float64,
	increment float64,
	center_x float64,
	center_y float64) ([]byte, error) {

	// handle any lingering anonymization
	// This is a bit confusing see comments
	//patient_name := "" // empty patient name/id will be OMITTED, causing ReconCT to inject the original values into the job
	//patient_id := ""   // empty patient name/id will be OMITTED, causing ReconCT to inject the original values into the job

	alg := algorithm
	dose := dose_pct
	st := slice_thickness

	var noise_simulation *NoiseSimulation = nil
	if dose != 100.0 {
		noise_simulation = &NoiseSimulation{
			NoiseSimulationChecked:         true,
			NoiseSimulationItems:           "ExFactor",
			NoiseSimulationReductionFactor: int(dose),
		}
	}
	
	short_alg := strings.TrimPrefix(strings.ToLower(alg), "standard - ")
	
	fmt.Println(alg, " ",  short_alg)

	// This assumes that we're always passing
	// both locations if any (passing only one will cause a panic)
	var recon_range *ReconRange = nil
	if start_pos != nil || end_pos != nil {
		recon_range = &ReconRange{
			StartPosition: *start_pos,
			EndPosition:   *end_pos,
		}
	}

	if increment != 0 {
		recon_range.Increment = increment
	}

	r := ReconJobViewModel{
		ReconMode:            alg,
		ConvolutionKernel:    kernel,
		SliceWidth:           TwoDecimalFloat(st),
		SeriesDescription:    fmt.Sprintf("%s-%s-%.0f-%s-%.1f", patient_name, short_alg, dose, kernel, st),
		AnonymizeDicomImages: false,
		PatientName:          patient_name,
		PatientId:            patient_id,
		NoiseSimulation:      noise_simulation,
		CenterX:              center_x,
		CenterY:              center_y,
		ReconRange:           recon_range,
	}

	if fov != 0.0 {
		r.FieldOfView = fov
	}

	//config_name := fmt.Sprintf("%s-%0.1f-%s-%s-%0.2f.xml", ptr_filename, dose, alg, kernel, st)
	//image_output := filepath.Join(s.imageOutput, fmt.Sprintf("%.2f-%s-%.2f", dose, kernel, st))

	return xml.MarshalIndent(r, "", " ")

}

func exists(filepath string) bool {
	_, err := os.Stat(filepath)
	return err == nil
}

func GetShellCommand(exe_filepath, ptr_filepath, config_filepath, output_directory string) *exec.Cmd {

	// Dump all configured paths b/c windows is being a fuck
	fmt.Println("ReconCT.exe:  ", exe_filepath, exists(exe_filepath))
	fmt.Println("PTR path:     ", ptr_filepath, exists(ptr_filepath))
	fmt.Println("Config path:  ", config_filepath, exists(config_filepath))
	fmt.Println("Image output: ", output_directory, exists(output_directory))

	return exec.Command(exe_filepath,
		"-s",
		"-autoshutdown",
		fmt.Sprintf("-r:%s", ptr_filepath),
		fmt.Sprintf("-p:%s", config_filepath),
		fmt.Sprintf("-setting:ImageDirectory=%s", output_directory))
}
