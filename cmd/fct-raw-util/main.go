/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>
*/
package main

import (
	_ "net/http/pprof"

	"gitlab.com/freect/freect/cmd/fct-raw-util/cmd"
)

func main() {

	//c := make(chan struct{})
	//go func() {
	//	http.ListenAndServe("localhost:8080", nil)
	//}()

	cmd.Execute()

	//<-c
}
