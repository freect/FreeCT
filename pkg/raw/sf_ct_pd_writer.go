package raw

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"os"
	"sort"
	"strconv"

	log "github.com/sirupsen/logrus"

	dicom "gitlab.com/freect/go-dicom"
	"gitlab.com/freect/go-dicom/pkg/frame"
	"gitlab.com/freect/go-dicom/pkg/tag"
	"gitlab.com/freect/go-dicom/pkg/uid"
)

type SFCTPDWriter struct {
	filepath string
}

func NewSFCTPDWriter(filepath string) *SFCTPDWriter {
	return &SFCTPDWriter{
		filepath: filepath,
	}
}

func (d *SFCTPDWriter) String() string {
	return fmt.Sprintf("SFCTPD, %s", d.filepath)
}

// TODO: Add support for ScanMetadata
func (d *SFCTPDWriter) Write(r RawReader) error {
	log.Info("Beginning read process")
	p, g, m, err := r.Read(nil)
	if err != nil {
		return err
	}

	log.Info("Beginning conversion process")
	dcm_data, err := ProjectionToDatasetSFCTPD(p, g, m)
	if err != nil {
		return err
	}

	// Write SFCTPD file
	log.Info("Beginning write process")
	file, err := os.Create(d.filepath)
	if err != nil {
		log.Fatal("failed to create SFCTPD files: ", err)
	}
	defer file.Close()

	if err = dicom.Write(file, dcm_data); err != nil {
		log.Fatal("failed to write SFCTPD files: ", err)
	}

	return nil
}

// ProjectionToDatasetSFCTPD returns a new dicom.Dataset in SFCTPD format using metadata from the input
// ProjectionStack and ScanGeometry objects and assigns values to corresponding dicom attributes.
func ProjectionToDatasetSFCTPD(p ProjectionStack, g *ScanGeometry, m *ScanMetadata) (dicom.Dataset, error) {
	SOPClassUID, _ := dicom.NewElement(tag.MediaStorageSOPClassUID, []string{"1.2.840.10008.5.1.4.1.1.2"})
	SOPInstanceUID, _ := dicom.NewElement(tag.MediaStorageSOPInstanceUID, []string{"1.2.3.4.5.6.7"})
	TransferSyntaxUID, _ := dicom.NewElement(tag.TransferSyntaxUID, []string{uid.ExplicitVRLittleEndian})

	dcm_data := dicom.Dataset{
		Elements: []*dicom.Element{
			SOPClassUID,
			SOPInstanceUID,
			TransferSyntaxUID,
		},
	}

	//// Metadata
	// ImplementationClassUID
	e, err := dicom.NewElement(tag.ImplementationClassUID, []string{m.ImplementationClassUID})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// ImplementationVersionName
	e, err = dicom.NewElement(tag.ImplementationVersionName, []string{m.ImplementationVersion})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SourceApplicationEntityTitle
	e, err = dicom.NewElement(tag.SourceApplicationEntityTitle, []string{m.SourceApplication})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Skipped SOPClassUID and SOPInstanceUID for now...

	// StudyDate
	e, err = dicom.NewElement(tag.StudyDate, []string{m.StudyDate})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SeriesDate
	e, err = dicom.NewElement(tag.SeriesDate, []string{m.SeriesDate})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// ContentDate
	e, err = dicom.NewElement(tag.ContentDate, []string{m.ContentDate})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// StudyTime
	e, err = dicom.NewElement(tag.StudyTime, []string{m.StudyTime})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// ContentTime
	e, err = dicom.NewElement(tag.ContentTime, []string{m.ContentTime})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// AccessionNumber
	e, err = dicom.NewElement(tag.AccessionNumber, []string{m.AccessionNumber})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// ReferringPhysicianName
	e, err = dicom.NewElement(tag.ReferringPhysicianName, []string{m.ReferringPhysicianName})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SeriesDescription
	e, err = dicom.NewElement(tag.SeriesDescription, []string{m.SeriesDescription})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Skipping IrradiationEventUID for now; not sure if it's going to be an array type

	// CreatorVersionUID
	e, err = dicom.NewElement(tag.CreatorVersionUID, []string{m.CreatorVersionUID})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PatientName
	e, err = dicom.NewElement(tag.PatientName, []string{m.PatientName})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PatientID
	e, err = dicom.NewElement(tag.PatientID, []string{m.PatientID})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PatientBirthDate
	e, err = dicom.NewElement(tag.PatientBirthDate, []string{m.PatientBirthDate})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PatientSex
	e, err = dicom.NewElement(tag.PatientSex, []string{m.PatientSex})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PatientAge
	e, err = dicom.NewElement(tag.PatientAge, []string{m.PatientAge})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PatientIdentityRemoved
	e, err = dicom.NewElement(tag.PatientIdentityRemoved, []string{m.PatientIdentityRemoved})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// DeidentificationMethod
	e, err = dicom.NewElement(tag.DeidentificationMethod, []string{m.DeidentificationMethod})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// BodyPartExamined
	e, err = dicom.NewElement(tag.BodyPartExamined, []string{m.BodyPartExamined})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// ProtocolName
	e, err = dicom.NewElement(tag.ProtocolName, []string{m.ProtocolName})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// ExposureTime
	e, err = dicom.NewElement(tag.ExposureTime, []string{strconv.Itoa(m.ExposureTime)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// XRayTubeCurrent
	e, err = dicom.NewElement(tag.XrayTubeCurrent, []string{strconv.Itoa(m.XRayTubeCurrent)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PatientPosition
	e, err = dicom.NewElement(tag.PatientPosition, []string{m.PatientPosition})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// StudyInstanceUID
	e, err = dicom.NewElement(tag.StudyInstanceUID, []string{m.StudyInstanceUID})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SeriesInstanceUID
	e, err = dicom.NewElement(tag.SeriesInstanceUID, []string{m.SeriesInstanceUID})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// StudyID
	e, err = dicom.NewElement(tag.StudyID, []string{m.StudyID})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SeriesNumber
	e, err = dicom.NewElement(tag.SeriesNumber, []string{strconv.Itoa(m.SeriesNumber)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// InstanceNumber
	e, err = dicom.NewElement(tag.InstanceNumber, []string{strconv.Itoa(m.InstanceNumber)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// TimeStamp
	e, err = dicom.NewElement(tag.Timestamp, []float64{m.TimeStamp})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// FrameOfReferenceUID
	e, err = dicom.NewElement(tag.FrameOfReferenceUID, []string{m.FrameOfReferenceUID})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PhotometricInterpretation
	e, err = dicom.NewElement(tag.PhotometricInterpretation, []string{m.PhotometricInterpretation})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// HighBit
	e, err = dicom.NewElement(tag.HighBit, []int{m.HighBit})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PixelRepresentation
	e, err = dicom.NewElement(tag.PixelRepresentation, []int{m.PixelRepresentation})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SourceAngularPositionShift
	e, err = dicom.NewElement(tag.SourceAngularPositionShift, []float64{m.SourceAngularPositionShift})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SourceAxialPositionShift
	e, err = dicom.NewElement(tag.SourceAxialPositionShift, []float64{m.SourceAxialPositionShift})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SourceRadialDistanceShift
	e, err = dicom.NewElement(tag.SourceRadialDistanceShift, []float64{m.SourceRadialDistanceShift})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// NumberOfSpectra
	e, err = dicom.NewElement(tag.NumberofSpectra, []int{m.NumberOfSpectra})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SpectrumIndex
	e, err = dicom.NewElement(tag.SpectrumIndex, []int{m.SpectrumIndex})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PhotonStatistics
	e, err = dicom.NewElement(tag.PhotonStatistics, []float64{m.PhotonStatistics})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// BeamHardeningCorrectionFlag
	e, err = dicom.NewElement(tag.BeamHardeningCorrectionFlag, []string{m.BeamHardeningCorrectionFlag})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// GainCorrectionFlag
	e, err = dicom.NewElement(tag.GainCorrectionFlag, []string{m.GainCorrectionFlag})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// DarkFieldCorrectionFlag
	e, err = dicom.NewElement(tag.DarkFieldCorrectionFlag, []string{m.DarkFieldCorrectionFlag})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// FlatFieldCorrectionFlag
	e, err = dicom.NewElement(tag.FlatFieldCorrectionFlag, []string{m.FlatFieldCorrectionFlag})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// BadPixelCorrectionFlag
	e, err = dicom.NewElement(tag.BadPixelCorrectionFlag, []string{m.BadPixelCorrectionFlag})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// ScatterCorrectionFlag
	e, err = dicom.NewElement(tag.ScatterCorrectionFlag, []string{m.ScatterCorrectionFlag})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// LogFlag
	e, err = dicom.NewElement(tag.LogFlag, []string{m.LogFlag})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	//// Dataset
	// Modality
	e, err = dicom.NewElement(tag.Modality, []string{"CT"})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Manufacturer
	e, err = dicom.NewElement(tag.Manufacturer, []string{g.Manufacturer})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Data Collection Diameter (Acquisition Field of View)
	e, err = dicom.NewElement(tag.DataCollectionDiameter, []string{strconv.FormatFloat(g.AcquisitionFieldOfView, 'f', -1, 64)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Spiral Pitch Factor (Pitch)
	e, err = dicom.NewElement(tag.SpiralPitchFactor, []float64{m.SpiralPitchFactor})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Rows
	e, err = dicom.NewElement(tag.Rows, []int{p[0].NumRows})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Columns
	e, err = dicom.NewElement(tag.Columns, []int{p[0].NumChannels})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Bits Allocated
	e, err = dicom.NewElement(tag.BitsAllocated, []int{16})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Bits Stored
	e, err = dicom.NewElement(tag.BitsStored, []int{16})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Samples Per Pixel
	e, err = dicom.NewElement(tag.SamplesPerPixel, []int{1})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Number of Frames
	e, err = dicom.NewElement(tag.NumberOfFrames, []string{strconv.Itoa(g.NumProjectionsTotal)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Detector Element Transverse Spacing
	e, err = dicom.NewElement(tag.DetectorElementTransverseSpacing, []float64{g.DetectorTransverseSpacing})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Detector Element Axial Spacing
	e, err = dicom.NewElement(tag.DetectorElementAxialSpacing, []float64{g.DetectorAxialSpacing})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Detector Shape
	e, err = dicom.NewElement(tag.DetectorShape, []string{g.DetectorShape})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Detector Rows
	e, err = dicom.NewElement(tag.NumberofDetectorRows, []int{g.DetectorRows})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Detector Columns
	e, err = dicom.NewElement(tag.NumberofDetectorColumns, []int{g.DetectorChannels})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Distance from Source to Isocenter
	e, err = dicom.NewElement(tag.DetectorFocalCenterRadialDistance, []float64{g.DistSourceToIsocenter})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Distance from Source to Detector
	e, err = dicom.NewElement(tag.ConstantRadialDistance, []float64{g.DistSourceToDetector})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Detector Central Element
	e, err = dicom.NewElement(tag.DetectorCentralElement, []float64{g.DetectorCentralChannel, g.DetectorCentralRow})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Flying Focal Spot Mode
	e, err = dicom.NewElement(tag.FlyingFocalSpotMode, []string{g.FlyingFocalSpotMode})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Number of Source Angular Steps
	e, err = dicom.NewElement(tag.NumberofSourceAngularSteps, []int{g.ProjectionsPerRotation})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Type of Projection Data
	e, err = dicom.NewElement(tag.TypeofProjectionData, []string{g.ScanType})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Type of Projection Geometry
	e, err = dicom.NewElement(tag.TypeofProjectionGeometry, []string{g.ProjectionGeometry})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Water Attenuation Coefficient (likely just HU Calibration Factor, will change later)
	e, err = dicom.NewElement(tag.WaterAttenuationCoefficient, []string{strconv.FormatFloat(g.HUCalibrationFactor, 'f', -1, 64)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Tube Angle Table
	buf := new(bytes.Buffer)
	for _, proj := range p {
		if err := binary.Write(buf, binary.LittleEndian, proj.TubeAngle); err != nil {
			return dcm_data, nil
		}
	}

	e, err = dicom.NewElement(tag.DetectorFocalCenterAngularPositionTable, buf.Bytes())
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Table Position Table
	buf = new(bytes.Buffer)
	for _, proj := range p {
		if err := binary.Write(buf, binary.LittleEndian, proj.TablePosition); err != nil {
			return dcm_data, nil
		}
	}

	e, err = dicom.NewElement(tag.DetectorFocalCenterAxialPositionTable, buf.Bytes())
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Rescale Intercept
	e, err = dicom.NewElement(tag.RescaleIntercept, []string{strconv.FormatFloat(m.RescaleIntercept, 'f', -1, 64)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Rescale Slope
	e, err = dicom.NewElement(tag.RescaleSlope, []string{strconv.FormatFloat(m.RescaleSlope, 'f', -1, 64)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Pixel Data
	proj_pixel_data := make([][]int, len(p))

	for i, proj := range p {
		if len(proj.DataInt) > 0 {
			proj_pixel_data[i] = proj.DataInt
			continue
		}
		proj_pixel_data[i] = proj.RescaleToInt(m.RescaleSlope, m.RescaleIntercept)
	}

	proj_info := dicom.PixelDataInfo{
		IsEncapsulated: true,
		Frames:         make([]frame.Frame, 0, len(p)),
	}

	for _, slice_px := range proj_pixel_data {
		buf := new(bytes.Buffer)
		for _, px := range slice_px {
			if err := binary.Write(buf, binary.LittleEndian, uint16(px)); err != nil {
				return dcm_data, err
			}
		}
		curr_frame := frame.Frame{
			Encapsulated: true,
			EncapsulatedData: frame.EncapsulatedFrame{
				Data: buf.Bytes(),
			},
		}
		proj_info.Frames = append(proj_info.Frames, curr_frame)
	}

	e, err = dicom.NewElement(tag.PixelData, proj_info)
	if err != nil {
		return dcm_data, err
	}
	e.ValueLength = tag.VLUndefinedLength // We do this because encapsulated pixel data has an undefined length

	dcm_data.Elements = append(dcm_data.Elements, e)

	// To stay compliant with the DICOM standard, we include the following attributes.
	// Most DICOM readers will not enforce the standard if these are missing, but they
	// are included for completeness.

	// Extended Offset Table Lengths
	offset_table_lengths := make([]uint64, len(proj_info.Frames))
	for i, curr_frame := range proj_info.Frames {
		frame_length := len(curr_frame.EncapsulatedData.Data)
		offset_table_lengths[i] = uint64(frame_length + frame_length%8)
	}

	e, err = dicom.NewElement(tag.ExtendedOffsetTableLengths, serialize_uint64_array_to_bytes(offset_table_lengths))
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Extended Offset Table
	offset_table := []uint64{0}
	for i, length := range offset_table_lengths[:len(offset_table_lengths)-1] {
		offset_table = append(offset_table, offset_table[i]+length+8) // Add 8 because item tag and VL take up 8 bytes per item
	}

	e, err = dicom.NewElement(tag.ExtendedOffsetTable, serialize_uint64_array_to_bytes(offset_table))
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Sort elements by tag
	sort.Slice(dcm_data.Elements, func(i, j int) bool {
		compare := dcm_data.Elements[i].Tag.Compare(dcm_data.Elements[j].Tag)
		switch compare {
		case -1:
			return true
		case 1:
			return false
		default:
			log.Fatal("ERROR: element of same tag appears twice in DICOM dataset: ", dcm_data.Elements[i].Tag.String(), dcm_data.Elements[i].Value.GetValue(), dcm_data.Elements[j].Tag.String(), dcm_data.Elements[j].Value.GetValue())
			return false
		}
	})

	return dcm_data, nil
}

func serialize_uint64_array_to_bytes(int_array []uint64) []byte {
	buf := new(bytes.Buffer)
	for _, num := range int_array {
		if err := binary.Write(buf, binary.LittleEndian, num); err != nil {
			return nil
		}
	}
	return buf.Bytes()
}
