package raw

import (
	"errors"
	"fmt"

	"strings"

	log "github.com/sirupsen/logrus"
	dicom "gitlab.com/freect/go-dicom"
	"gitlab.com/freect/go-dicom/pkg/tag"

	"gitlab.com/freect/freect/pkg/util"
)

var _ RawReader = &BattelleReader{}

// BattelleReader reads raw projection data from the "Battelle" format
// specified by Mayo Clinic.  It fulfills the RawReader interface.
type BattelleReader struct {
	//path     string
	lastRead int
	fileList []string
	reader   FileStoreReader
}

func NewBattelleReader(r FileStoreReader) (*BattelleReader, error) {
	file_list, err := r.ListFiles()
	if err != nil {
		return nil, err
	}

	dr := &BattelleReader{
		lastRead: -1,
		fileList: file_list,
		reader:   r,
	}

	return dr, nil
}

// readDatasetFromStore is a helper function to reduce boilerplate
func (d *BattelleReader) readDatasetFromStore(curr_file string) (dicom.Dataset, error) {

	file, err := d.reader.Open(curr_file)
	if err != nil {
		return dicom.Dataset{}, err
	}

	info, err := d.reader.Stat(curr_file)
	if err != nil {
		return dicom.Dataset{}, err
	}

	dcm_data, err := dicom.Parse(file, info.Size(), nil)
	if err != nil {
		return dicom.Dataset{}, err
	}

	err = file.Close()
	if err != nil {
		return dicom.Dataset{}, err
	}

	return dcm_data, nil
}

func (d *BattelleReader) String() string {

	return strings.Join([]string{d.reader.String(), "battelle"}, ",")
}

func (d *BattelleReader) ReadProjection() (*Projection, error) {
	// Increment lastRead to the next projection
	d.lastRead++

	// No more images
	if d.lastRead == len(d.fileList) {
		return nil, nil
	}

	// Get current file from fileList and read dataset from filestore
	curr_file := d.fileList[d.lastRead]

	dcm_data, err := d.readDatasetFromStore(curr_file)
	if err != nil {
		return nil, err
	}

	p := &Projection{
		Index: d.lastRead,
	}

	// Extract metadata
	e, err := dcm_data.FindElementByTag(tag.NumberofDetectorRows)
	if err != nil {
		return nil, err
	}
	p.NumRows = int(e.Value.GetValue().([]int)[0])

	e, err = dcm_data.FindElementByTag(tag.NumberofDetectorColumns)
	if err != nil {
		return nil, err
	}
	p.NumChannels = int(e.Value.GetValue().([]int)[0])

	e, err = dcm_data.FindElementByTag(tag.DetectorFocalCenterAngularPosition)
	if err != nil {
		return nil, err
	}
	p.TubeAngle = float64(e.Value.GetValue().([]float64)[0])

	e, err = dcm_data.FindElementByTag(tag.DetectorFocalCenterAxialPosition)
	if err != nil {
		return nil, err
	}
	p.TablePosition = float64(e.Value.GetValue().([]float64)[0])

	// Extract central detector (a little more complicated)
	var central_detector []float64 = nil
	if err := readFloatsTag(&dcm_data, tag.DetectorCentralElement, &central_detector); err != nil {
		return nil, err
	}

	if len(central_detector) != 2 {
		err_msg := fmt.Sprintf("expected two coordinates for central detector, but found %d", len(central_detector))
		return nil, errors.New(err_msg)
	}
	p.CentralChannel = (central_detector)[0]
	p.CentralRow = (central_detector)[1]

	// Get the detector size
	if err := readFloatTag(&dcm_data, tag.DetectorElementTransverseSpacing, &p.ChannelWidth); err != nil {
		return nil, err
	}

	if err := readFloatTag(&dcm_data, tag.DetectorElementAxialSpacing, &p.RowWidth); err != nil {
		return nil, err
	}

	// Extract pixel data
	p.Data, err = readPixelData(&dcm_data, p.NumRows, p.NumChannels)
	if err != nil {
		return nil, err
	}

	// Extract int pixel data (delete later)
	p.DataInt, err = readPixelDataInt(&dcm_data, p.NumRows, p.NumChannels)
	if err != nil {
		return nil, err
	}

	return p, nil
}

func (d *BattelleReader) ReadGeometry() (*ScanGeometry, error) {

	s := &ScanGeometry{}

	// Grab our first file in the file list
	curr_file := d.fileList[0]

	// Load the projection
	dcm_data, err := d.readDatasetFromStore(curr_file)
	if err != nil {
		return nil, err
	}

	// Parse the necessary tags
	if err := readStringTag(dcm_data, tag.Manufacturer, &s.Manufacturer); err != nil {
		return nil, err
	}

	if err := readIntTag(&dcm_data, tag.NumberofDetectorRows, &s.DetectorRows); err != nil {
		return nil, err
	}

	if err := readIntTag(&dcm_data, tag.NumberofDetectorColumns, &s.DetectorChannels); err != nil {
		return nil, err
	}

	if err := readFloatTag(&dcm_data, tag.DetectorElementTransverseSpacing, &s.DetectorTransverseSpacing); err != nil {
		return nil, err
	}

	if err := readFloatTag(&dcm_data, tag.DetectorElementAxialSpacing, &s.DetectorAxialSpacing); err != nil {
		return nil, err
	}

	if err := readStringTag(dcm_data, tag.DetectorShape, &s.DetectorShape); err != nil {
		return nil, err
	}

	if err := readFloatTag(&dcm_data, tag.DetectorFocalCenterRadialDistance, &s.DistSourceToIsocenter); err != nil {
		return nil, err
	}

	if err := readFloatTag(&dcm_data, tag.ConstantRadialDistance, &s.DistSourceToDetector); err != nil {
		return nil, err
	}

	if err := readIntTag(&dcm_data, tag.NumberofSourceAngularSteps, &s.ProjectionsPerRotation); err != nil {
		return nil, err
	}

	if err := readStringTag(dcm_data, tag.TypeofProjectionData, &s.ScanType); err != nil {
		return nil, err
	}

	if err := readStringTag(dcm_data, tag.FlyingFocalSpotMode, &s.FlyingFocalSpotMode); err != nil {
		return nil, err
	}

	if err := readStringTag(dcm_data, tag.TypeofProjectionGeometry, &s.ProjectionGeometry); err != nil {
		return nil, err
	}

	if err := readFloatTag(&dcm_data, tag.DetectorFocalCenterAxialPosition, &s.StartTablePosition); err != nil {
		return nil, err
	}

	s.AcquisitionFieldOfView, err = readDecimalString(&dcm_data, tag.DataCollectionDiameter)
	if err != nil {
		return nil, err
	}

	// There seems to be some disagreement between the dictionary
	// provided with the LDCT PD data and the publication about what
	// this tag is. We try to read either one but don't fail if we can't find it.
	var HUCalibrationTag = tag.Tag{Group: 0x0018, Element: 0x0061}
	s.HUCalibrationFactor, err = readDecimalString(&dcm_data, HUCalibrationTag)
	if err != nil {
		s.HUCalibrationFactor, err = readDecimalString(&dcm_data, tag.WaterAttenuationCoefficient)
		if err != nil {
			log.Warn("Could not find HU Calibration Factor in dataset.  HU rescaling will not be possible.")
		}
	}

	s.CollimatedSliceWidth = s.DistSourceToIsocenter * s.DetectorAxialSpacing / s.DistSourceToDetector

	pitch := 0.0
	if err := readFloatTag(&dcm_data, tag.SpiralPitchFactor, &pitch); err != nil {
		fmt.Println(err)
		return nil, err
	}
	s.TableFeedPerRotation = pitch * s.CollimatedSliceWidth * float64(s.DetectorRows)

	var central_detector []float64 = nil
	if err := readFloatsTag(&dcm_data, tag.DetectorCentralElement, &central_detector); err != nil {
		return nil, err
	}

	if len(central_detector) != 2 {
		err_msg := fmt.Sprintf("expected two coordinates for central detector, but found %d", len(central_detector))
		return nil, errors.New(err_msg)
	}
	s.DetectorCentralChannel = (central_detector)[0]
	s.DetectorCentralRow = (central_detector)[1]

	s.NumProjectionsTotal = len(d.fileList)

	// Grab our last file in the file list
	last_file := d.fileList[len(d.fileList)-1]

	dcm_data, err = d.readDatasetFromStore(last_file)
	if err != nil {
		return nil, err
	}

	if err := readFloatTag(&dcm_data, tag.DetectorFocalCenterAxialPosition, &s.EndTablePosition); err != nil {
		return nil, err
	}

	return s, nil
}

func (d *BattelleReader) ReadMetadata() (*ScanMetadata, error) {

	m := &ScanMetadata{}

	// Grab our first file in the file list
	curr_file := d.fileList[0]

	// Load the projection
	dcm_data, err := d.readDatasetFromStore(curr_file)
	if err != nil {
		return nil, err
	}

	// Parse the necessary tags; required tags should not use an "optional" tag read function
	if err := readStringTagOptional(&dcm_data, tag.ImplementationClassUID, &m.ImplementationClassUID); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.ImplementationVersionName, &m.ImplementationVersion); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.SourceApplicationEntityTitle, &m.SourceApplication); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.SOPClassUID, &m.SOPClassUID); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.SOPInstanceUID, &m.SOPInstanceUID); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.StudyDate, &m.StudyDate); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.SeriesDate, &m.SeriesDate); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.ContentDate, &m.ContentDate); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.StudyTime, &m.StudyTime); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.ContentTime, &m.ContentTime); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.AccessionNumber, &m.AccessionNumber); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.ReferringPhysicianName, &m.ReferringPhysicianName); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.SeriesDescription, &m.SeriesDescription); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.IrradiationEventUID, &m.IrradiationEventUID); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.CreatorVersionUID, &m.CreatorVersionUID); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.PatientName, &m.PatientName); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.PatientID, &m.PatientID); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.PatientBirthDate, &m.PatientBirthDate); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.PatientSex, &m.PatientSex); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.PatientAge, &m.PatientAge); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.PatientIdentityRemoved, &m.PatientIdentityRemoved); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.DeidentificationMethod, &m.DeidentificationMethod); err != nil {
		return nil, err
	}

	m.RescaleIntercept, err = readDecimalString(&dcm_data, tag.RescaleIntercept)
	if err != nil {
		return nil, err
	}

	m.RescaleSlope, err = readDecimalString(&dcm_data, tag.RescaleSlope)
	if err != nil {
		return nil, err
	}

	if err := readFloatTag(&dcm_data, tag.SpiralPitchFactor, &m.SpiralPitchFactor); err != nil {
		return nil, err
	}

	//log.Info(m.RescaleIntercept, m.RescaleSlope, m.SpiralPitchFactor)

	if err := readStringTag(dcm_data, tag.BodyPartExamined, &m.BodyPartExamined); err != nil {
		return nil, err
	}

	tmp, err := readDecimalString(&dcm_data, tag.KVP)
	if err != nil {
		return nil, err
	}
	m.KVP = int(tmp)

	if err := readStringTagOptional(&dcm_data, tag.ProtocolName, &m.ProtocolName); err != nil {
		return nil, err
	}

	tmp, err = readDecimalString(&dcm_data, tag.ExposureTime)
	if err != nil {
		return nil, err
	}
	m.ExposureTime = int(tmp)

	tmp, err = readDecimalString(&dcm_data, tag.XrayTubeCurrent)
	if err != nil {
		return nil, err
	}
	m.XRayTubeCurrent = int(tmp)

	if err := readStringTag(dcm_data, tag.PatientPosition, &m.PatientPosition); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.StudyInstanceUID, &m.StudyInstanceUID); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.SeriesInstanceUID, &m.SeriesInstanceUID); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.StudyID, &m.StudyID); err != nil {
		return nil, err
	}

	tmp, err = readDecimalString(&dcm_data, tag.SeriesNumber)
	if err != nil {
		return nil, err
	}
	m.SeriesNumber = int(tmp)

	tmp, err = readDecimalString(&dcm_data, tag.InstanceNumber)
	if err != nil {
		return nil, err
	}
	m.InstanceNumber = int(tmp)

	if err := readStringTagOptional(&dcm_data, tag.FrameOfReferenceUID, &m.FrameOfReferenceUID); err != nil {
		return nil, err
	}

	if err := readStringTag(dcm_data, tag.PhotometricInterpretation, &m.PhotometricInterpretation); err != nil {
		return nil, err
	}

	if err := readIntTag(&dcm_data, tag.HighBit, &m.HighBit); err != nil {
		return nil, err
	}

	if err := readIntTag(&dcm_data, tag.PixelRepresentation, &m.PixelRepresentation); err != nil {
		return nil, err
	}

	if err := readFloatTag(&dcm_data, tag.SourceAngularPositionShift, &m.SourceAngularPositionShift); err != nil {
		return nil, err
	}

	if err := readFloatTag(&dcm_data, tag.SourceAxialPositionShift, &m.SourceAxialPositionShift); err != nil {
		return nil, err
	}

	if err := readFloatTag(&dcm_data, tag.SourceRadialDistanceShift, &m.SourceRadialDistanceShift); err != nil {
		return nil, err
	}

	if err := readIntTagOptional(&dcm_data, tag.NumberofSpectra, &m.NumberOfSpectra); err != nil {
		return nil, err
	}

	if err := readIntTagOptional(&dcm_data, tag.SpectrumIndex, &m.SpectrumIndex); err != nil {
		return nil, err
	}

	if err := readFloatTagOptional(&dcm_data, tag.PhotonStatistics, &m.PhotonStatistics); err != nil {
		return nil, err
	}

	if err := readFloatTagOptional(&dcm_data, tag.Timestamp, &m.TimeStamp); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.BeamHardeningCorrectionFlag, &m.BeamHardeningCorrectionFlag); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.GainCorrectionFlag, &m.GainCorrectionFlag); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.DarkFieldCorrectionFlag, &m.DarkFieldCorrectionFlag); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.FlatFieldCorrectionFlag, &m.FlatFieldCorrectionFlag); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.BadPixelCorrectionFlag, &m.BadPixelCorrectionFlag); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.ScatterCorrectionFlag, &m.ScatterCorrectionFlag); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&dcm_data, tag.LogFlag, &m.LogFlag); err != nil {
		return nil, err
	}

	return m, nil
}

func (d *BattelleReader) ReadN(N int) ([]*Projection, *ScanGeometry, *ScanMetadata, error) {

	projs := make([]*Projection, N)
	for i := range projs {
		if i == N {
			break
		}

		util.LogUpdate(i+1, N, 10, "reading projections")

		p, err := d.ReadProjection()
		if err != nil {
			return nil, nil, nil, err
		}
		projs[i] = p
	}

	g, err := d.ReadGeometry()
	if err != nil {
		return nil, nil, nil, err
	}

	g.NumProjectionsTotal = N

	m, err := d.ReadMetadata()
	if err != nil {
		return nil, nil, nil, err
	}

	return projs, g, m, nil
}

func (d *BattelleReader) Read(geom_overrides *ScanGeometry) ([]*Projection, *ScanGeometry, *ScanMetadata, error) {
	projs := make([]*Projection, len(d.fileList))
	for i := range projs {
		p, err := d.ReadProjection()
		if err != nil {
			return nil, nil, nil, err
		}

		projs[i] = p
	}

	g, err := d.ReadGeometry()
	if err != nil {
		return nil, nil, nil, err
	}

	m, err := d.ReadMetadata()
	if err != nil {
		return nil, nil, nil, err
	}

	if geom_overrides == nil {
		return projs, g, m, nil
	}

	// TODO: Somehow alert the user that overrides are being utilized?
	g = geom_overrides
	for _, p := range projs {
		p.CentralRow = geom_overrides.DetectorCentralRow
		p.CentralChannel = geom_overrides.DetectorCentralChannel
		p.ChannelWidth = geom_overrides.DetectorTransverseSpacing
		p.RowWidth = geom_overrides.DetectorAxialSpacing
	}

	return projs, g, m, nil
}
