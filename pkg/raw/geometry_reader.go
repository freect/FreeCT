package raw

type ScanGeometryReader interface {
	ReadGeometry() (*ScanGeometry, error)
}
