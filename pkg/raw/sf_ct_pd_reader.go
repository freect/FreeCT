package raw

import (
	"errors"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	dicom "gitlab.com/freect/go-dicom"
	"gitlab.com/freect/go-dicom/pkg/tag"
)

var _ RawReader = &SFCTPDReader{}

type SFCTPDReader struct {
	datasetLoaded bool
	filepath      string
	lastRead      int
	data          dicom.Dataset
}

func NewSFCTPDReader(filepath string) (*SFCTPDReader, error) {

	r := &SFCTPDReader{
		filepath: filepath,
		lastRead: -1,
	}

	return r, nil
}

func (d *SFCTPDReader) loadDataset() error {

	log.Info("loading dataset")

	file, err := os.Open(d.filepath)
	if err != nil {
		return err
	}

	defer file.Close()

	info, err := file.Stat()
	if err != nil {
		return err
	}

	// Parse returns a copy of the loaded dataset.  Why not a pointer?
	// dataset is a slice of pointers to elements, so I don't know
	// that the actual copy is too expensive, but stills seems
	// unnecessary unless there's a good reason to not return the
	// pointer...
	dcm_data, err := dicom.Parse(file, info.Size(), nil)
	if err != nil {
		return err
	}

	d.datasetLoaded = true
	d.data = dcm_data

	log.Info("done loading dataset")

	return nil

}

func (d *SFCTPDReader) String() string {
	return fmt.Sprintf("SFCTPD format reader, %s", d.filepath)
}

func (d *SFCTPDReader) ReadProjection() (*Projection, error) {

	if !d.datasetLoaded {
		err := d.loadDataset()
		if err != nil {
			return nil, err
		}
	}

	// ReadProjection returns projection data, number of projections, and an error.
	// ReadProjection allows only one SFCTPD file

	// Increment lastRead to match current projection
	d.lastRead++

	// No more projections
	if num_projs, err := readNumberOfProjections(&d.data); d.lastRead == num_projs {
		return nil, nil
	} else if err != nil {
		return nil, fmt.Errorf("error reading number of projections: %v", err)
	}

	p := &Projection{
		Index: d.lastRead,
	}

	// Extract metadata
	e, err := d.data.FindElementByTag(tag.NumberofDetectorRows)
	if err != nil {
		return nil, err
	}
	p.NumRows = int(e.Value.GetValue().([]int)[0])

	e, err = d.data.FindElementByTag(tag.NumberofDetectorColumns)
	if err != nil {
		return nil, err
	}
	p.NumChannels = int(e.Value.GetValue().([]int)[0])

	// TODO: FIX THIS PART SO THE ELEMENT IS NOT MISSING

	// Extract the tube angle
	e, err = d.data.FindElementByTag(tag.DetectorFocalCenterAngularPositionTable)
	if err != nil {
		return nil, err
	}

	p.TubeAngle = float64(e.Value.GetValue().([]float64)[d.lastRead])

	// Extract the table position
	e, err = d.data.FindElementByTag(tag.DetectorFocalCenterAxialPositionTable)
	if err != nil {
		return nil, err
	}
	p.TablePosition = float64(e.Value.GetValue().([]float64)[d.lastRead])

	// Extract central detector
	var central_detector []float64 = nil
	if err := readFloatsTag(&d.data, tag.DetectorCentralElement, &central_detector); err != nil {
		return nil, err
	}

	if len(central_detector) != 2 {
		err_msg := fmt.Sprintf("expected two coordinates for central detector, but found %d", len(central_detector))
		return nil, errors.New(err_msg)
	}
	p.CentralChannel = (central_detector)[0]
	p.CentralRow = (central_detector)[1]

	// Get the detector size
	if err := readFloatTag(&d.data, tag.DetectorElementTransverseSpacing, &p.ChannelWidth); err != nil {
		return nil, err
	}

	if err := readFloatTag(&d.data, tag.DetectorElementAxialSpacing, &p.RowWidth); err != nil {
		return nil, err
	}

	// Extract pixel data
	p.Data, err = readPixelDataFrame(&d.data, p.NumRows, p.NumChannels, d.lastRead)
	if err != nil {
		return nil, err
	}

	return p, nil
}

func (d *SFCTPDReader) ReadGeometry() (*ScanGeometry, error) {

	if !d.datasetLoaded {
		err := d.loadDataset()
		if err != nil {
			return nil, err
		}
	}

	log.Info("geometry")

	s := &ScanGeometry{}

	// Parse the necessary tags
	err := readStringTag(d.data, tag.Manufacturer, &s.Manufacturer)
	if err != nil {
		return nil, err
	}

	if err := readIntTag(&d.data, tag.NumberofDetectorRows, &s.DetectorRows); err != nil {
		return nil, err
	}

	if err := readIntTag(&d.data, tag.NumberofDetectorColumns, &s.DetectorChannels); err != nil {
		return nil, err
	}

	if err := readFloatTag(&d.data, tag.DetectorElementTransverseSpacing, &s.DetectorTransverseSpacing); err != nil {
		return nil, err
	}

	if err := readFloatTag(&d.data, tag.DetectorElementAxialSpacing, &s.DetectorAxialSpacing); err != nil {
		return nil, err
	}

	if err := readStringTag(d.data, tag.DetectorShape, &s.DetectorShape); err != nil {
		return nil, err
	}

	if err := readFloatTag(&d.data, tag.DetectorFocalCenterRadialDistance, &s.DistSourceToIsocenter); err != nil {
		return nil, err
	}

	if err := readFloatTag(&d.data, tag.ConstantRadialDistance, &s.DistSourceToDetector); err != nil {
		return nil, err
	}

	if err := readIntTag(&d.data, tag.NumberofSourceAngularSteps, &s.ProjectionsPerRotation); err != nil {
		return nil, err
	}

	if err := readStringTag(d.data, tag.TypeofProjectionData, &s.ScanType); err != nil {
		return nil, err
	}

	if err := readStringTag(d.data, tag.FlyingFocalSpotMode, &s.FlyingFocalSpotMode); err != nil {
		return nil, err
	}

	if err := readStringTag(d.data, tag.TypeofProjectionGeometry, &s.ProjectionGeometry); err != nil {
		return nil, err
	}

	if err := readFloatArrayTag(&d.data, tag.DetectorFocalCenterAxialPositionTable, &s.StartTablePosition, 0); err != nil {
		return nil, err
	}

	s.AcquisitionFieldOfView, err = readDecimalString(&d.data, tag.DataCollectionDiameter)
	if err != nil {
		return nil, err
	}

	// There seems to be some disagreement between the dictionary
	// provided with the LDCT PD data and the publication about what
	// this tag is. We try to read either one but don't fail if we can't find it.
	var HUCalibrationTag = tag.Tag{Group: 0x0018, Element: 0x0061}
	s.HUCalibrationFactor, err = readDecimalString(&d.data, HUCalibrationTag)
	if err != nil {
		s.HUCalibrationFactor, err = readDecimalString(&d.data, tag.WaterAttenuationCoefficient)
		if err != nil {
			log.Warn("Could not find HU Calibration Factor in dataset.  HU rescaling will not be possible.")
		}
	}

	s.CollimatedSliceWidth = s.DistSourceToIsocenter * s.DetectorAxialSpacing / s.DistSourceToDetector

	pitch := 0.0
	if err := readFloatTag(&d.data, tag.SpiralPitchFactor, &pitch); err != nil {
		fmt.Println(err)
		return nil, err
	}
	s.TableFeedPerRotation = pitch * s.CollimatedSliceWidth * float64(s.DetectorRows)

	var central_detector []float64 = nil
	if err := readFloatsTag(&d.data, tag.DetectorCentralElement, &central_detector); err != nil {
		return nil, err
	}

	if len(central_detector) != 2 {
		err_msg := fmt.Sprintf("expected two coordinates for central detector, but found %d", len(central_detector))
		return nil, errors.New(err_msg)
	}
	s.DetectorCentralChannel = (central_detector)[0]
	s.DetectorCentralRow = (central_detector)[1]

	s.NumProjectionsTotal, err = readNumberOfProjections(&d.data)
	if err != nil {
		return nil, fmt.Errorf("failed to get the number of projections: %v", err)
	}

	if err := readFloatArrayTag(&d.data, tag.DetectorFocalCenterAxialPositionTable, &s.EndTablePosition, s.NumProjectionsTotal-1); err != nil {
		return nil, err
	}

	log.Info("done")

	return s, nil
}

// TODO: Implement ReadMetadata
func (d *SFCTPDReader) ReadMetadata() (*ScanMetadata, error) {

	if !d.datasetLoaded {
		err := d.loadDataset()
		if err != nil {
			return nil, err
		}
	}

	log.Info("metadata")

	m := &ScanMetadata{}

	// Parse the necessary tags; required tags should not use an "optional" tag read function
	if err := readStringTagOptional(&d.data, tag.ImplementationClassUID, &m.ImplementationClassUID); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.ImplementationVersionName, &m.ImplementationVersion); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.SourceApplicationEntityTitle, &m.SourceApplication); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.SOPClassUID, &m.SOPClassUID); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.SOPInstanceUID, &m.SOPInstanceUID); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.StudyDate, &m.StudyDate); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.SeriesDate, &m.SeriesDate); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.ContentDate, &m.ContentDate); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.StudyTime, &m.StudyTime); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.ContentTime, &m.ContentTime); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.AccessionNumber, &m.AccessionNumber); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.ReferringPhysicianName, &m.ReferringPhysicianName); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.SeriesDescription, &m.SeriesDescription); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.IrradiationEventUID, &m.IrradiationEventUID); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.CreatorVersionUID, &m.CreatorVersionUID); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.PatientName, &m.PatientName); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.PatientID, &m.PatientID); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.PatientBirthDate, &m.PatientBirthDate); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.PatientSex, &m.PatientSex); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.PatientAge, &m.PatientAge); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.PatientIdentityRemoved, &m.PatientIdentityRemoved); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.DeidentificationMethod, &m.DeidentificationMethod); err != nil {
		return nil, err
	}

	var err error
	m.RescaleIntercept, err = readDecimalString(&d.data, tag.RescaleIntercept)
	if err != nil {
		return nil, err
	}

	m.RescaleSlope, err = readDecimalString(&d.data, tag.RescaleSlope)
	if err != nil {
		return nil, err
	}

	if err := readFloatTag(&d.data, tag.SpiralPitchFactor, &m.SpiralPitchFactor); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.BodyPartExamined, &m.BodyPartExamined); err != nil {
		return nil, err
	}

	tmp, err := readDecimalStringOptional(&d.data, tag.KVP)
	if err != nil {
		return nil, err
	}
	m.KVP = int(tmp)

	if err := readStringTagOptional(&d.data, tag.ProtocolName, &m.ProtocolName); err != nil {
		return nil, err
	}

	// Not sure if this will actually be an array of values or not...
	tmp, err = readDecimalStringOptional(&d.data, tag.ExposureTime)
	if err != nil {
		return nil, err
	}
	m.ExposureTime = int(tmp)

	// Not sure if this will actually be an array of values or not... alla tube current modulation
	tmp, err = readDecimalStringOptional(&d.data, tag.XrayTubeCurrent)
	if err != nil {
		return nil, err
	}
	m.XRayTubeCurrent = int(tmp)

	if err := readStringTagOptional(&d.data, tag.PatientPosition, &m.PatientPosition); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.StudyInstanceUID, &m.StudyInstanceUID); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.SeriesInstanceUID, &m.SeriesInstanceUID); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.StudyID, &m.StudyID); err != nil {
		return nil, err
	}

	tmp, err = readDecimalStringOptional(&d.data, tag.SeriesNumber)
	if err != nil {
		return nil, err
	}
	m.SeriesNumber = int(tmp)

	tmp, err = readDecimalStringOptional(&d.data, tag.InstanceNumber)
	if err != nil {
		return nil, err
	}
	m.InstanceNumber = int(tmp)

	if err := readStringTagOptional(&d.data, tag.FrameOfReferenceUID, &m.FrameOfReferenceUID); err != nil {
		return nil, err
	}

	if err := readStringTag(d.data, tag.PhotometricInterpretation, &m.PhotometricInterpretation); err != nil {
		return nil, err
	}

	if err := readIntTag(&d.data, tag.PixelRepresentation, &m.PixelRepresentation); err != nil {
		return nil, err
	}

	if err := readIntTagOptional(&d.data, tag.HighBit, &m.HighBit); err != nil {
		return nil, err
	}

	// Not sure if the following eight attributes will also have to go into arrays
	if err := readFloatTagOptional(&d.data, tag.SourceAngularPositionShift, &m.SourceAngularPositionShift); err != nil {
		return nil, err
	}

	if err := readFloatTagOptional(&d.data, tag.SourceAxialPositionShift, &m.SourceAxialPositionShift); err != nil {
		return nil, err
	}

	if err := readFloatTagOptional(&d.data, tag.SourceRadialDistanceShift, &m.SourceRadialDistanceShift); err != nil {
		return nil, err
	}

	if err := readIntTagOptional(&d.data, tag.NumberofSpectra, &m.NumberOfSpectra); err != nil {
		return nil, err
	}

	if err := readIntTagOptional(&d.data, tag.SpectrumIndex, &m.SpectrumIndex); err != nil {
		return nil, err
	}

	if err := readFloatTagOptional(&d.data, tag.PhotonStatistics, &m.PhotonStatistics); err != nil {
		return nil, err
	}

	if err := readFloatTagOptional(&d.data, tag.Timestamp, &m.TimeStamp); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.BeamHardeningCorrectionFlag, &m.BeamHardeningCorrectionFlag); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.GainCorrectionFlag, &m.GainCorrectionFlag); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.DarkFieldCorrectionFlag, &m.DarkFieldCorrectionFlag); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.FlatFieldCorrectionFlag, &m.FlatFieldCorrectionFlag); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.BadPixelCorrectionFlag, &m.BadPixelCorrectionFlag); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.ScatterCorrectionFlag, &m.ScatterCorrectionFlag); err != nil {
		return nil, err
	}

	if err := readStringTagOptional(&d.data, tag.LogFlag, &m.LogFlag); err != nil {
		return nil, err
	}

	log.Info("done")

	return m, nil
}

func (d *SFCTPDReader) Read(geom_overrides *ScanGeometry) ([]*Projection, *ScanGeometry, *ScanMetadata, error) {

	if !d.datasetLoaded {
		err := d.loadDataset()
		if err != nil {
			return nil, nil, nil, err
		}
	}

	m, err := d.ReadMetadata()
	if err != nil {
		return nil, nil, nil, err
	}

	//log.Info("Finished reading scan metadata")
	num_proj, err := readNumberOfProjections(&d.data)
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to get the number of projections: %v", err)
	}

	p := make([]*Projection, num_proj)
	for i := range p {
		proj, err := d.ReadProjection()
		if err != nil {
			return nil, nil, nil, err
		}

		p[i] = proj
	}

	//log.Info("Finished reading projections")

	// Read the scan geometry and override if necessary
	g, err := d.ReadGeometry()
	if err != nil {
		return nil, nil, nil, err
	}

	//log.Info("Finished reading scan geometry")

	if geom_overrides == nil {
		return p, g, m, nil
		//return p, g, nil, nil
	}

	// TODO: Somehow alert the user that overrides are being utilized?
	g = geom_overrides
	for _, proj := range p {
		proj.CentralRow = geom_overrides.DetectorCentralRow
		proj.CentralChannel = geom_overrides.DetectorCentralChannel
		proj.ChannelWidth = geom_overrides.DetectorTransverseSpacing
		proj.RowWidth = geom_overrides.DetectorAxialSpacing
	}

	return p, g, m, nil
}
