package raw

import (
	"encoding/binary"
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/freect/go-dicom"
	"gitlab.com/freect/go-dicom/pkg/tag"
)

// Functions that reduce boilerplate when reading different types of DICOM attributes/tags

func readDecimalString(data *dicom.Dataset, t tag.Tag) (float64, error) {
	e, err := data.FindElementByTag(t)
	if err != nil {
		//log.Infof("Could not read tag: %v", t)
		return 0.0, err
	}

	string_val := e.Value.GetValue().([]string)[0]

	val, err := strconv.ParseFloat(string_val, 64)
	if err != nil {
		return 0.0, err
	}

	return val, nil
}

func readStringTag(d dicom.Dataset, t tag.Tag, s *string) error {

	e, err := d.FindElementByTag(t)
	if err != nil {
		return err
	}

	strs, ok := e.Value.GetValue().([]string)
	if !ok {
		err_msg := fmt.Sprintf("string assertion failed for tag: %s", t.String())
		return errors.New(err_msg)
	}

	if len(strs) < 1 {
		err_msg := fmt.Sprintf("no strings returned for tag: %s", t.String())
		return errors.New(err_msg)
	}

	*s = strs[0]

	return nil
}

func readIntTag(d *dicom.Dataset, t tag.Tag, i *int) error {

	e, err := d.FindElementByTag(t)
	if err != nil {
		return err
	}

	ints, ok := e.Value.GetValue().([]int)
	if !ok {
		err_msg := fmt.Sprintf("int assertion failed for tag: %s", t.String())
		return errors.New(err_msg)
	}

	if len(ints) < 1 {
		err_msg := fmt.Sprintf("no ints returned for tag: %s", t.String())
		return errors.New(err_msg)
	}

	*i = ints[0]

	return nil
}

func readFloatTag(d *dicom.Dataset, t tag.Tag, f *float64) error {

	e, err := d.FindElementByTag(t)
	if err != nil {
		return err
	}

	floats, ok := e.Value.GetValue().([]float64)
	if !ok {
		err_msg := fmt.Sprintf("float64 assertion failed for tag: %s", t.String())
		return errors.New(err_msg)
	}

	if len(floats) < 1 {
		err_msg := fmt.Sprintf("no float64s returned for tag: %s", t.String())
		return errors.New(err_msg)
	}

	*f = floats[0]

	return nil
}

func readFloatsTag(d *dicom.Dataset, t tag.Tag, f *[]float64) error {

	e, err := d.FindElementByTag(t)
	if err != nil {
		return err
	}

	floats, ok := e.Value.GetValue().([]float64)
	if !ok {
		err_msg := fmt.Sprintf("float64 assertion failed for tag: %s", t.String())
		return errors.New(err_msg)
	}

	if len(floats) == 0 {
		err_msg := fmt.Sprintf("no float64s returned for tag: %s", t.String())
		return errors.New(err_msg)
	}

	*f = floats

	return nil
}

func readFloatArrayTag(d *dicom.Dataset, t tag.Tag, f *float64, index int) error {
	// Reads a float value into an element with the desired tag from a slice of floats
	// in the SFCTPD dataset.

	e, err := d.FindElementByTag(t)
	if err != nil {
		return err
	}

	floats, ok := e.Value.GetValue().([]float64)
	if !ok {
		return fmt.Errorf("float64 assertion failed for tag: %s", t.String())
	}

	if len(floats) == 0 {
		return fmt.Errorf("no float64s returned for tag: %s", t.String())
	}

	*f = floats[index]

	return nil
}

func readPixelData(dcm_data *dicom.Dataset, detector_rows int, detector_channels int) (final_proj []float32, err error) {
	// Extract rescale values for pixel data
	rescale_intercept, err := readDecimalString(dcm_data, tag.RescaleIntercept)
	if err != nil {
		return nil, err
	}

	rescale_slope, err := readDecimalString(dcm_data, tag.RescaleSlope)
	if err != nil {
		return nil, err
	}

	// Extract pixel data
	e, err := dcm_data.FindElementByTag(tag.PixelData)
	if err != nil {
		return nil, err
	}
	imageInfo := e.Value.GetValue().(dicom.PixelDataInfo)

	for _, f := range imageInfo.Frames {
		nf, err := f.GetNativeFrame()
		if err != nil {
			return nil, err
		}

		final_proj = make([]float32, nf.Rows*nf.Cols)

		if len(nf.Data[0]) > 1 {
			return nil, errors.New("multiple channels found in projection data (should be grayscale)")
		}

		// Determine if pixel data was incorrectly stored transposed
		if nf.Rows == detector_channels && nf.Cols == detector_rows {
			for i := 0; i < detector_rows; i++ {
				for j := 0; j < detector_channels; j++ {
					dest_idx := j + i*detector_channels
					src_idx := i + j*detector_rows
					final_proj[dest_idx] = float32(rescale_intercept) + float32(rescale_slope)*float32(nf.Data[src_idx][0])
				}
			}
		} else {
			for i := range final_proj {
				final_proj[i] = float32(rescale_intercept) + float32(rescale_slope)*float32(nf.Data[i][0])
			}
		}

	}

	return final_proj, nil
}

func readPixelDataFrame(dcm_data *dicom.Dataset, detector_rows int, detector_channels int, frame_idx int) (final_proj []float32, err error) {

	// I think we're doing addition effort here that we don't
	// necessarily need to do.

	// Extract rescale values for pixel data
	rescale_intercept, err := readDecimalString(dcm_data, tag.RescaleIntercept)
	if err != nil {
		return nil, err
	}

	rescale_slope, err := readDecimalString(dcm_data, tag.RescaleSlope)
	if err != nil {
		return nil, err
	}

	// Extract pixel data from appropriate frame
	e, err := dcm_data.FindElementByTag(tag.PixelData)
	if err != nil {
		return nil, err
	}
	image_info := e.Value.GetValue().(dicom.PixelDataInfo)
	curr_frame := image_info.Frames[frame_idx]

	// Parse through Encapsulated data
	if image_info.IsEncapsulated {
		ef, err := curr_frame.GetEncapsulatedFrame()
		if err != nil {
			return nil, err
		}

		final_proj = make([]float32, detector_channels*detector_rows)
		for i := 0; i < detector_channels*detector_rows; i++ {
			final_proj[i] = float32(rescale_intercept) + float32(rescale_slope)*float32(binary.LittleEndian.Uint16(ef.Data[i*2:(i+1)*2]))
		}

		return final_proj, nil
	}

	// Parse through Native data
	nf, err := curr_frame.GetNativeFrame()
	if err != nil {
		return nil, err
	}

	// If needed, this allocation could probably be optimized away
	final_proj = make([]float32, nf.Rows*nf.Cols)

	if len(nf.Data[0]) > 1 {
		return nil, errors.New("multiple channels found in projection data (should be grayscale)")
	}

	// Determine if pixel data was incorrectly stored transposed
	if nf.Rows == detector_channels && nf.Cols == detector_rows {
		for i := 0; i < detector_rows; i++ {
			for j := 0; j < detector_channels; j++ {
				dest_idx := j + i*detector_channels
				src_idx := i + j*detector_rows
				final_proj[dest_idx] = float32(rescale_intercept) + float32(rescale_slope)*float32(nf.Data[src_idx][0])
			}
		}
	} else {
		for i := range final_proj {
			final_proj[i] = float32(rescale_intercept) + float32(rescale_slope)*float32(nf.Data[i][0])
		}
	}

	return final_proj, nil
}

func readPixelDataInt(dcm_data *dicom.Dataset, detector_rows int, detector_channels int) (final_proj []int, err error) {
	// Extract pixel data
	e, err := dcm_data.FindElementByTag(tag.PixelData)
	if err != nil {
		return nil, err
	}
	imageInfo := e.Value.GetValue().(dicom.PixelDataInfo)

	for _, f := range imageInfo.Frames {
		nf, err := f.GetNativeFrame()
		if err != nil {
			return nil, err
		}

		final_proj = make([]int, nf.Rows*nf.Cols)

		if len(nf.Data[0]) > 1 {
			return nil, errors.New("multiple channels found in projection data (should be grayscale)")
		}

		// Determine if pixel data was incorrectly stored transposed
		if nf.Rows == detector_channels && nf.Cols == detector_rows {
			for i := 0; i < detector_rows; i++ {
				for j := 0; j < detector_channels; j++ {
					dest_idx := j + i*detector_channels
					src_idx := i + j*detector_rows
					final_proj[dest_idx] = nf.Data[src_idx][0]
				}
			}
		} else {
			for i := range final_proj {
				final_proj[i] = nf.Data[i][0]
			}
		}

	}

	return final_proj, nil
}

func readNumberOfProjections(dcm_data *dicom.Dataset) (int, error) {

	e, err := dcm_data.FindElementByTag(tag.NumberOfFrames)
	if err != nil {
		return 0, err
	}

	value_str, ok := e.Value.GetValue().([]string)
	if !ok {
		return 0, fmt.Errorf("[]string assertion failed for tag: %s", tag.NumberOfFrames.String())
	}

	value, err := strconv.Atoi(value_str[0])
	if err != nil {
		return 0, fmt.Errorf("failed to convert string to integer for %s tag: %v", tag.NumberOfFrames.String(), err)
	}

	return value, nil
}

// Convenience functions useful for metadata parsing

func readStringTagOptional(data *dicom.Dataset, t tag.Tag, s *string) error {
	if err := readStringTag(*data, t, s); err != nil {
		//log.Infof("\twarning: optional read string error for tag %v; setting to empty string: %v", t, err)
		*s = ""
		return nil
	}

	return nil
}

func readFloatTagOptional(d *dicom.Dataset, t tag.Tag, f *float64) error {
	if err := readFloatTag(d, t, f); err != nil {
		//log.Infof("\twarning: optional read string error for tag %v; setting to 0: %v", t, err)
		*f = 0.0
		return nil
	}

	return nil
}

func readIntTagOptional(d *dicom.Dataset, t tag.Tag, i *int) error {
	if err := readIntTag(d, t, i); err != nil {
		//log.Infof("\twarning: optional read integer error for tag %v; setting to 0: %v", t, err)
		*i = 0
		return nil
	}

	return nil
}

func readDecimalStringOptional(data *dicom.Dataset, t tag.Tag) (float64, error) {
	val, err := readDecimalString(data, t)
	if err != nil {
		//log.Infof("\twarning: optional read decimal string error for tag %v; setting to 0: %v", t, err)
		return 0.0, nil
	}

	return val, nil
}
