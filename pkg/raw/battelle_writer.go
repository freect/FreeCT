package raw

import (
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"strconv"

	log "github.com/sirupsen/logrus"

	dicom "gitlab.com/freect/go-dicom"
	"gitlab.com/freect/go-dicom/pkg/frame"
	"gitlab.com/freect/go-dicom/pkg/tag"
	"gitlab.com/freect/go-dicom/pkg/uid"
)

type BattelleWriter struct {
	path      string
	fileStore FileStoreWriter
}

func NewBattelleWriter(f FileStoreWriter, wdir string) *BattelleWriter {
	return &BattelleWriter{
		path:      wdir,
		fileStore: f,
	}
}

func (d *BattelleWriter) String() string {
	return fmt.Sprintf("Battelle writer to %s", d.path)
}

// TODO: Add support for ScanMetadata
func (d *BattelleWriter) Write(r RawReader) error {
	p, g, m, err := r.Read(nil)
	if err != nil {
		return err
	}

	for i := range p {
		// Convert from Projection to a dicom dataset
		dcm_data, err := ProjectionToDatasetBattelle(p, g, m, i)
		if err != nil {
			return err
		}

		// Write Battelle file
		if _, err = os.Stat(d.path); os.IsNotExist(err) {
			os.Mkdir(d.path, os.ModePerm) // Skipping over error handling for now
		}

		filename := filepath.Join(d.path, fmt.Sprintf("%06d%s", i+1, ".dcm"))
		file, err := os.Create(filename)
		if err != nil {
			log.Fatal("Failed to create Battelle files: ", err)
		}
		defer file.Close()

		if err = dicom.Write(file, dcm_data); err != nil {
			log.Fatal("Failed to write Battelle files: ", err)
		}
	}

	return nil
}

// ProjectionToDatasetBattelle returns a dicom.Dataset in Battelle format using metadata from the input
// ProjectionStack and ScanGeometry objects and assigns values to corresponding dicom attributes.
func ProjectionToDatasetBattelle(p ProjectionStack, g *ScanGeometry, m *ScanMetadata, proj_idx int) (dicom.Dataset, error) {

	SOPClassUID, _ := dicom.NewElement(tag.MediaStorageSOPClassUID, []string{"1.2.840.10008.5.1.4.1.1.2"})
	SOPInstanceUID, _ := dicom.NewElement(tag.MediaStorageSOPInstanceUID, []string{"1.2.3.4.5.6.7"})
	TransferSyntaxUID, _ := dicom.NewElement(tag.TransferSyntaxUID, []string{uid.ExplicitVRLittleEndian})

	dcm_data := dicom.Dataset{
		Elements: []*dicom.Element{
			SOPClassUID,
			SOPInstanceUID,
			TransferSyntaxUID,
		},
	}

	// Metadata
	// ImplementationClassUID
	e, err := dicom.NewElement(tag.ImplementationClassUID, []string{m.ImplementationClassUID})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// ImplementationVersionName
	e, err = dicom.NewElement(tag.ImplementationVersionName, []string{m.ImplementationVersion})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SourceApplicationEntityTitle
	e, err = dicom.NewElement(tag.SourceApplicationEntityTitle, []string{m.SourceApplication})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SOPClassUID
	e, err = dicom.NewElement(tag.SOPClassUID, []string{m.SOPClassUID})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SOPInstanceUID
	e, err = dicom.NewElement(tag.SOPInstanceUID, []string{m.SOPInstanceUID})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// StudyDate
	e, err = dicom.NewElement(tag.StudyDate, []string{m.StudyDate})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SeriesDate
	e, err = dicom.NewElement(tag.SeriesDate, []string{m.SeriesDate})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// ContentDate
	e, err = dicom.NewElement(tag.ContentDate, []string{m.ContentDate})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// StudyTime
	e, err = dicom.NewElement(tag.StudyTime, []string{m.StudyTime})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// ContentTime
	e, err = dicom.NewElement(tag.ContentTime, []string{m.ContentTime})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// AccessionNumber
	e, err = dicom.NewElement(tag.AccessionNumber, []string{m.AccessionNumber})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// ReferringPhysicianName
	e, err = dicom.NewElement(tag.ReferringPhysicianName, []string{m.ReferringPhysicianName})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SeriesDescription
	e, err = dicom.NewElement(tag.SeriesDescription, []string{m.SeriesDescription})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// IrradiationEventUID
	e, err = dicom.NewElement(tag.IrradiationEventUID, []string{m.IrradiationEventUID})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// CreatorVersionUID
	e, err = dicom.NewElement(tag.CreatorVersionUID, []string{m.CreatorVersionUID})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PatientName
	e, err = dicom.NewElement(tag.PatientName, []string{m.PatientName})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PatientID
	e, err = dicom.NewElement(tag.PatientID, []string{m.PatientID})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PatientBirthDate
	e, err = dicom.NewElement(tag.PatientBirthDate, []string{m.PatientBirthDate})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PatientSex
	e, err = dicom.NewElement(tag.PatientSex, []string{m.PatientSex})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PatientAge
	e, err = dicom.NewElement(tag.PatientAge, []string{m.PatientAge})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PatientIdentityRemoved
	e, err = dicom.NewElement(tag.PatientIdentityRemoved, []string{m.PatientIdentityRemoved})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// DeidentificationMethod
	e, err = dicom.NewElement(tag.DeidentificationMethod, []string{m.DeidentificationMethod})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// BodyPartExamined
	e, err = dicom.NewElement(tag.BodyPartExamined, []string{m.BodyPartExamined})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// ProtocolName
	e, err = dicom.NewElement(tag.ProtocolName, []string{m.ProtocolName})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// ExposureTime
	e, err = dicom.NewElement(tag.ExposureTime, []string{strconv.Itoa(m.ExposureTime)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// XRayTubeCurrent
	e, err = dicom.NewElement(tag.XrayTubeCurrent, []string{strconv.Itoa(m.XRayTubeCurrent)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PatientPosition
	e, err = dicom.NewElement(tag.PatientPosition, []string{m.PatientPosition})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// StudyInstanceUID
	e, err = dicom.NewElement(tag.StudyInstanceUID, []string{m.StudyInstanceUID})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SeriesInstanceUID
	e, err = dicom.NewElement(tag.SeriesInstanceUID, []string{m.SeriesInstanceUID})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// StudyID
	e, err = dicom.NewElement(tag.StudyID, []string{m.StudyID})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SeriesNumber
	e, err = dicom.NewElement(tag.SeriesNumber, []string{strconv.Itoa(m.SeriesNumber)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// TimeStamp
	e, err = dicom.NewElement(tag.Timestamp, []float64{m.TimeStamp})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// FrameOfReferenceUID
	e, err = dicom.NewElement(tag.FrameOfReferenceUID, []string{m.FrameOfReferenceUID})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PhotometricInterpretation
	e, err = dicom.NewElement(tag.PhotometricInterpretation, []string{m.PhotometricInterpretation})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// HighBit
	e, err = dicom.NewElement(tag.HighBit, []int{m.HighBit})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PixelRepresentation
	e, err = dicom.NewElement(tag.PixelRepresentation, []int{m.PixelRepresentation})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SourceAngularPositionShift
	e, err = dicom.NewElement(tag.SourceAngularPositionShift, []float64{m.SourceAngularPositionShift})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SourceAxialPositionShift
	e, err = dicom.NewElement(tag.SourceAxialPositionShift, []float64{m.SourceAxialPositionShift})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SourceRadialDistanceShift
	e, err = dicom.NewElement(tag.SourceRadialDistanceShift, []float64{m.SourceRadialDistanceShift})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// NumberOfSpectra
	e, err = dicom.NewElement(tag.NumberofSpectra, []int{m.NumberOfSpectra})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// SpectrumIndex
	e, err = dicom.NewElement(tag.SpectrumIndex, []int{m.SpectrumIndex})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// PhotonStatistics
	e, err = dicom.NewElement(tag.PhotonStatistics, []float64{m.PhotonStatistics})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// BeamHardeningCorrectionFlag
	e, err = dicom.NewElement(tag.BeamHardeningCorrectionFlag, []string{m.BeamHardeningCorrectionFlag})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// GainCorrectionFlag
	e, err = dicom.NewElement(tag.GainCorrectionFlag, []string{m.GainCorrectionFlag})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// DarkFieldCorrectionFlag
	e, err = dicom.NewElement(tag.DarkFieldCorrectionFlag, []string{m.DarkFieldCorrectionFlag})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// FlatFieldCorrectionFlag
	e, err = dicom.NewElement(tag.FlatFieldCorrectionFlag, []string{m.FlatFieldCorrectionFlag})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// BadPixelCorrectionFlag
	e, err = dicom.NewElement(tag.BadPixelCorrectionFlag, []string{m.BadPixelCorrectionFlag})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// ScatterCorrectionFlag
	e, err = dicom.NewElement(tag.ScatterCorrectionFlag, []string{m.ScatterCorrectionFlag})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// LogFlag
	e, err = dicom.NewElement(tag.LogFlag, []string{m.LogFlag})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Dataset
	// Modality
	e, err = dicom.NewElement(tag.Modality, []string{"CT"})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Manufacturer
	e, err = dicom.NewElement(tag.Manufacturer, []string{g.Manufacturer})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Data Collection Diameter (Acquisition Field of View)
	e, err = dicom.NewElement(tag.DataCollectionDiameter, []string{strconv.FormatFloat(g.AcquisitionFieldOfView, 'f', -1, 64)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Spiral Pitch Factor (Pitch)
	e, err = dicom.NewElement(tag.SpiralPitchFactor, []float64{(g.TableFeedPerRotation * g.DistSourceToDetector) / (g.CollimatedSliceWidth * float64(g.DetectorRows) * g.DetectorAxialSpacing * g.DistSourceToIsocenter)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Rows
	e, err = dicom.NewElement(tag.Rows, []int{p[0].NumChannels})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Columns
	e, err = dicom.NewElement(tag.Columns, []int{p[0].NumRows})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Bits Allocated
	e, err = dicom.NewElement(tag.BitsAllocated, []int{16})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Bits Stored
	e, err = dicom.NewElement(tag.BitsStored, []int{16})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Samples Per Pixel
	e, err = dicom.NewElement(tag.SamplesPerPixel, []int{1})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Detector Element Transverse Spacing
	e, err = dicom.NewElement(tag.DetectorElementTransverseSpacing, []float64{g.DetectorTransverseSpacing})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Detector Element Axial Spacing
	e, err = dicom.NewElement(tag.DetectorElementAxialSpacing, []float64{g.DetectorAxialSpacing})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Detector Shape
	e, err = dicom.NewElement(tag.DetectorShape, []string{g.DetectorShape})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Detector Rows
	e, err = dicom.NewElement(tag.NumberofDetectorRows, []int{g.DetectorRows})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Detector Columns
	e, err = dicom.NewElement(tag.NumberofDetectorColumns, []int{g.DetectorChannels})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Distance from Source to Isocenter
	e, err = dicom.NewElement(tag.DetectorFocalCenterRadialDistance, []float64{g.DistSourceToIsocenter})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Distance from Source to Detector
	e, err = dicom.NewElement(tag.ConstantRadialDistance, []float64{g.DistSourceToDetector})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Detector Central Element
	e, err = dicom.NewElement(tag.DetectorCentralElement, []float64{g.DetectorCentralChannel, g.DetectorCentralRow})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Flying Focal Spot Mode
	e, err = dicom.NewElement(tag.FlyingFocalSpotMode, []string{g.FlyingFocalSpotMode})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Number of Source Angular Steps
	e, err = dicom.NewElement(tag.NumberofSourceAngularSteps, []int{g.ProjectionsPerRotation})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Type of Projection Data
	e, err = dicom.NewElement(tag.TypeofProjectionData, []string{g.ScanType})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Type of Projection Geometry
	e, err = dicom.NewElement(tag.TypeofProjectionGeometry, []string{g.ProjectionGeometry})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Water Attenuation Coefficient (likely just HU Calibration Factor, will change later)
	e, err = dicom.NewElement(tag.WaterAttenuationCoefficient, []string{strconv.FormatFloat(g.HUCalibrationFactor, 'f', -1, 64)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Check if pixel data needs to be rescaled
	rescale_slope := 1.0
	rescale_intercept := 0.0

	if !p.IsIntPixelData() {
		rescale_slope, rescale_intercept = ComputeRescaleToInt16(p)
	}

	// Rescale Intercept
	e, err = dicom.NewElement(tag.RescaleIntercept, []string{strconv.FormatFloat(rescale_intercept, 'f', -1, 64)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Rescale Slope
	e, err = dicom.NewElement(tag.RescaleSlope, []string{strconv.FormatFloat(rescale_slope, 'f', -1, 64)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Instance Number
	e, err = dicom.NewElement(tag.InstanceNumber, []string{strconv.Itoa(proj_idx)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Tube Angle
	e, err = dicom.NewElement(tag.DetectorFocalCenterAngularPosition, []float64{(p[proj_idx].TubeAngle)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Table Position
	e, err = dicom.NewElement(tag.DetectorFocalCenterAxialPosition, []float64{(p[proj_idx].TablePosition)})
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// // Pixel Data
	proj_pixel_data := make([][]int, len(p[proj_idx].Data))

	untrans_proj := p[proj_idx].RescaleToInt(rescale_slope, rescale_intercept)

	for i := 0; i < g.DetectorRows; i++ {
		for j := 0; j < g.DetectorChannels; j++ {
			dest_idx := j + i*g.DetectorChannels
			src_idx := i + j*g.DetectorRows
			proj_pixel_data[src_idx] = []int{untrans_proj[dest_idx]}
		}
	}

	proj_info := dicom.PixelDataInfo{
		IsEncapsulated: false,
		Frames: []frame.Frame{
			{
				Encapsulated: false,
				NativeData: frame.NativeFrame{
					BitsPerSample: 16,
					Rows:          p[proj_idx].NumRows,
					Cols:          p[proj_idx].NumChannels,
					Data:          proj_pixel_data,
				},
			},
		},
	}

	e, err = dicom.NewElement(tag.PixelData, proj_info)
	if err != nil {
		return dcm_data, err
	}
	dcm_data.Elements = append(dcm_data.Elements, e)

	// Sort elements by tag
	sort.Slice(dcm_data.Elements, func(i, j int) bool {
		compare := dcm_data.Elements[i].Tag.Compare(dcm_data.Elements[j].Tag)
		switch compare {
		case -1:
			return true
		case 1:
			return false
		default:
			log.Fatal("ERROR: element of same tag appears twice in DICOM dataset: ", dcm_data.Elements[i].Tag.String(), dcm_data.Elements[i].Value.GetValue(), dcm_data.Elements[j].Tag.String(), dcm_data.Elements[j].Value.GetValue())
			return false
		}
	})

	return dcm_data, nil
}
