package image

import (
	// log "github.com/sirupsen/logrus"
	// "os"
	"fmt"

	dicom "gitlab.com/freect/go-dicom"
)

// WORK IN PROGRESS -JG

// DICOMReader reads CT images. It fulfills the ImageReader interface.
type DICOMReader struct {
	lastRead int
	fileList []string
	reader   ImageStoreReader
}

func NewDICOMReader(r ImageStoreReader) (*DICOMReader, error) {
	file_list, err := r.ListFiles()
	if err != nil {
		return nil, err
	}

	dr := &DICOMReader{
		lastRead: -1,
		fileList: file_list,
		reader:   r,
	}

	return dr, nil
}

func (d *DICOMReader) readDatasetFromStore(curr_file string) (dicom.Dataset, error) {
	file, err := d.reader.Open(curr_file)
	if err != nil {
		return dicom.Dataset{}, err
	}

	info, err := d.reader.Stat(curr_file)
	if err != nil {
		return dicom.Dataset{}, err
	}

	dcm_data, err := dicom.Parse(file, info.Size(), nil)
	if err != nil {
		return dicom.Dataset{}, err
	}

	err = file.Close()
	if err != nil {
		return dicom.Dataset{}, err
	}

	return dcm_data, nil
}

func (d *DICOMReader) String() string {
	return fmt.Sprintln("DICOM reader")
}
